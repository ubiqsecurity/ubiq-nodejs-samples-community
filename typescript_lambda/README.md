
# Getting Started

1. Create Lambda function
    - Runtime: Node.js 18x
1. Configure following Ubiq [environment variables](https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-config)
    - UBIQ_ACCESS_KEY_ID
    - UBIQ_SECRET_SIGNING_KEY
    - UBIQ_SECRET_CRYPTO_ACCESS_KEY
1. Build example
    - ``npm install``
    - ``npm run prebuild && npm run build && npm run postbuild``
1. Upload dist/index.zip to Lambda
1. Run a test

## Example Event JSON

``` json
{
  "plainText": "12-31-1999",
  "datasetName": "BIRTH_DATE"
}
```
