import ubiq from 'ubiq-security';
import { Context, APIGatewayProxyEvent , APIGatewayProxyResult } from 'aws-lambda';

interface EncryptionEvent extends APIGatewayProxyEvent {
  plainText: string,
  datasetName: string
}

export const handler = async (event: EncryptionEvent, context: Context): Promise<APIGatewayProxyResult> => {
  console.log(`Event: ${JSON.stringify(event, null, 2)}`);
  console.log(`Context: ${JSON.stringify(context, null, 2)}`);

  return {
      statusCode: 200,
      body: JSON.stringify({
          plainText: event.plainText,
          cipherText: await encrypt(event.datasetName, event.plainText),
      }),
  };
};

async function encrypt(datasetName: string, plainText: string): Promise<string> { 
  // reads credentials from following lambda environment variables
  // UBIQ_ACCESS_KEY_ID, UBIQ_SECRET_SIGNING_KEY, UBIQ_SECRET_CRYPTO_ACCESS_KEY
  const credentials = new ubiq.Credentials();

  // ensure credentials have been found and loaded properly
  if (!credentials.access_key_id || !credentials.secret_signing_key || !credentials.secret_crypto_access_key) {
    console.log('  Unable to load credentials file properly.');
    console.log('  Check credentials file pathname and selected profile');
  }

  const ubiqEncryptDecrypt = new ubiq.fpeEncryptDecrypt.FpeEncryptDecrypt({ 
    ubiqCredentials: credentials, 
    ubiqConfiguration: null
  });

  // encrypt
  const ct = await ubiqEncryptDecrypt.EncryptAsync(datasetName, plainText);

  return ct;
}
