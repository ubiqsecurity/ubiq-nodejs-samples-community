A simple application that demonstrates how to use the Ubiq library from a NodeJS service with a React frontend for FPE/structured data encryption. 

## Installation

Clone Project 
```bash
CD <project-directory>
```
Set environment variables in .env (refer to .env.sample)

```bash
PORT=<your_port>
UBIQ_ACCESS_KEY_ID=<your_access_key_id>
UBIQ_SECRET_SIGNING_KEY=<your_secret_signing_key>
UBIQ_SECRET_CRYPTO_ACCESS_KEY=<your_secret_crypto_access_key>
```
Install npm dependencies
```bash
npm install
cd client/
npm install
```

Build and start docker container 
```bash
docker compose up
```
Start local servers
```bash
npm run dev
cd client/
npm start
```

## Usage
Open browser  

Client: http://localhost:3000/  
API: http://localhost:<your_port> 