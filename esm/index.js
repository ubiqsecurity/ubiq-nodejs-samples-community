import ubiq from 'ubiq-security';

async function main() { 
  const plainText = '12-31-1999';

  try {
    // reads credentials from default profile in ~/.ubiq/credentials
    const credentials = new ubiq.ConfigCredentials();

    // ensure credentials have been found and loaded properly
    if (!credentials.access_key_id || !credentials.secret_signing_key || !credentials.secret_crypto_access_key) {
      console.log('  Unable to load credentials file properly.');
      console.log('  Check credentials file pathname and selected profile');
      process.exit();
    }

    const ubiqEncryptDecrypt = new ubiq.fpeEncryptDecrypt.FpeEncryptDecrypt({ ubiqCredentials: credentials });

    // encrypt
    const ct = await ubiqEncryptDecrypt.EncryptAsync("BIRTH_DATE", plainText);

    console.log(`encrpyted '${plainText}' to ${ct}`);

    // decrypt
    const pt = await ubiqEncryptDecrypt.DecryptAsync("BIRTH_DATE", ct);

    console.log(`decrpyted '${ct}' to ${pt}`);

    ubiqEncryptDecrypt.close();
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
  
  process.exit(0);
}

main();
