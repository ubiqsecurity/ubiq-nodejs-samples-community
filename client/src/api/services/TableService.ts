import { faker } from '@faker-js/faker';

type database = 'mysql' | 'dynamodb';

export async function getEncryptedTableData(database: database) {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_BASE_URL}${database}`
    );
    return await response.json();
  } catch (err) {
    console.error(err);
  }
}

export async function getDecryptedTableData(database: database) {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_BASE_URL}${database}/decrypt`
    );
    return await response.json();
  } catch (err) {
    console.error(err);
  }
}

export async function addEncryptedTableData(database: database) {
  const contactOptions = ['phone', 'email'];
  const userPreferencesOptions = ['Light Mode', 'Dark Mode'];
  try {
    const fullName = faker.helpers.fake(
      '{{name.firstName}} {{name.middleName}} {{name.middleName}} {{name.lastName}}'
    );
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ssn_sensitive: faker.phone.number('###-##-####'),
        full_name_sensitive: fullName,
        user_preferences: userPreferencesOptions[Math.round(Math.random())],
        phone_number_sensitive: faker.phone.number('###-###-####'),
        contact_method: contactOptions[Math.round(Math.random())],
        email_sensitive: faker.internet
          .email()
          .toLocaleLowerCase()
          .replace('_', ''),
      }),
    };
    const response = await fetch(
      `${process.env.REACT_APP_BASE_URL}${database}`,
      requestOptions
    );
  } catch (err) {
    console.error(err);
  }
}
