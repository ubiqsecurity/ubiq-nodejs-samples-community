import { MantineProvider, Container, Title } from '@mantine/core';
import { TableContainer } from './containers/TableContainer/TableContainer';

function App() {
  return (
    <>
      <MantineProvider withGlobalStyles withNormalizeCSS>
        <Container
          size={'xl'}
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            gap: 40,
          }}
        >
          <Title>POC</Title>
          <TableContainer />
        </Container>
      </MantineProvider>
    </>
  );
}

export default App;
