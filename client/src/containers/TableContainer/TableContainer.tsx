import { useEffect, useState } from 'react';
import { Title } from '@mantine/core';
import { Controls } from '../../components/Controls';
import Table from '../../components/Table';
import { ColumnDefinitions } from './ColumnDefinitions';
import {
  addEncryptedTableData,
  getDecryptedTableData,
  getEncryptedTableData,
} from '../../api/services/TableService';

export function TableContainer() {
  const [isEncrypted, setIsEncrypted] = useState(true);
  const [isAddingRow, setIsAddingRow] = useState(false);
  const [rowData, setRowData] = useState<[] | undefined>();
  const [columnDefs, setColumnDefs] = useState(ColumnDefinitions);
  const [database, setDatabase] = useState<'mysql' | 'dynamodb'>('mysql');

  useEffect(() => {
    if (isEncrypted && !isAddingRow) {
      getEncryptedTableData(database).then((data) => {
        setRowData(data.data);
      });
    }
    if (!isEncrypted && !isAddingRow) {
      getDecryptedTableData(database).then((data) => setRowData(data.data));
    }
    if (isAddingRow) {
      addEncryptedTableData(database).then((_) => setIsAddingRow(false));
    }
  }, [isEncrypted, isAddingRow, database]);

  function handleEncryptDecryptChange(
    event: React.ChangeEvent<HTMLInputElement>
  ) {
    setRowData(undefined);
    setIsEncrypted(event.currentTarget.checked);
  }

  function handleAddRowClick() {
    setRowData(undefined);
    setIsAddingRow(true);
  }

  function handleDatabaseChange(event: React.ChangeEvent<HTMLInputElement>) {
    setRowData(undefined);
    setDatabase((prevState) => (prevState === 'mysql' ? 'dynamodb' : 'mysql'));
  }

  return (
    <>
      <Controls
        onEncryptDecryptChange={handleEncryptDecryptChange}
        onAddRowClick={handleAddRowClick}
        isEncrypted={isEncrypted}
        database={database}
        onDatabaseChange={handleDatabaseChange}
      />
      {rowData?.length === 0 ? (
        <Title order={2}>Click + Row</Title>
      ) : (
        <Table rowData={rowData} columnDefs={columnDefs} />
      )}
    </>
  );
}
