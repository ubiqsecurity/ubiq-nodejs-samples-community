export const ColumnDefinitions = [
  { headerName: 'ID', field: 'id', resizable: true, flex: 1 },
  {
    headerName: 'Full Name',
    field: 'full_name_sensitive',
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'SSN',
    field: 'ssn_sensitive',
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'Contact Method',
    field: 'contact_method',
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'Phone',
    field: 'phone_number_sensitive',
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'Email',
    field: 'email_sensitive',
    maxWidth: 184,
    resizable: true,
    flex: 1,
  },
  {
    headerName: 'User Preferences',
    field: 'user_preferences',
    maxWidth: 184,
    flex: 1,
  },
];
