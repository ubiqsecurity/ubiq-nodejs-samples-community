import { Button, Switch } from '@mantine/core';

interface ButtonsProps {
  isEncrypted: boolean;
  onEncryptDecryptChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onAddRowClick: () => void;
  database: 'mysql' | 'dynamodb';
  onDatabaseChange: (event: React.ChangeEvent<HTMLInputElement>) => void;

}
export function Controls(props: ButtonsProps) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        gap: 40,
      }}
    >
      <Switch
        size={'xl'}
        onLabel={'ENCRYPT'}
        offLabel={'DECRYPT'}
        checked={props.isEncrypted}
        onChange={props.onEncryptDecryptChange}
      />
      <Button
        size={'sm'}
        radius={'xl'}
        style={{ width: '104.81px', border: '1px solid #228be6' }}
        onClick={props.onAddRowClick}
      >
        + Row
      </Button>
      <Switch
        size={'xl'}
        onLabel={'MYSQL'}
        offLabel={'DYNAMODB'}
        value={props.database}
        checked={props.database === 'mysql'}
        onChange={props.onDatabaseChange}
      />
    </div>
  );
}
