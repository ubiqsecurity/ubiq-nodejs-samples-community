import { AgGridReact } from 'ag-grid-react';
import { Loader } from '@mantine/core';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-material.css';
import { ColDef } from 'ag-grid-community';

interface TableProps {
  rowData: any;
  columnDefs: Array<ColDef>;
}
export default function Table(props: TableProps) {
  return (
    <div
      style={{
        width: '100%',
      }}
    >
      <div className='ag-theme-material'>
        <AgGridReact
          rowData={props.rowData}
          columnDefs={props.columnDefs}
          domLayout={'autoHeight'}
          loadingOverlayComponent={Loader}
          noRowsOverlayComponent={Loader}
        />
      </div>
    </div>
  );
}
