# Ubiq Security test harness sample application to load a large data file
# and process both encrypt / decrypt operations and validate both the
# functionality but also the performance of the client library

## Documentation

See the [Node.js API docs][apidocs].

### Requirements

Node.js version 16 or later
npm version 6 or later

From within the example directory using [npm] or [yarn]

```console
$ cd ubiq-nodejs-samples-community/load_test
$ npm install
# or
$ yarn install
```
## Credentials file

Edit the credentials file with your account [Credentials][credentials] created using the [Ubiq Dashboard][dashboard].

## Usage

### Test Harness parameters


```
$ node load_test.js -h

Usage: load_test.js [-e max_avg_encrypt] [-d max_acg_decrypt] [-E max_total_encrypt] [-D max_total_decrypt] -i INPUT [-c CREDENTIALS] [-P PROFILE]
       Run performance tests and validate the cross language compatibility

Options:
  -V, --version                    output the version number
  -p, --print_errors               Print information regarding data validation errors (default: false)
  -e, --max_avg_encrypt <value>    Maximum allowed average encrypt time in microseconds (default: 0)
  -d, --max_avg_decrypt <value>    Maximum allowed average decrypt time in microseconds (default: 0)
  -E, --max_total_encrypt <value>  Maximum allowed total encrypt time in microseconds (default: 0)
  -D, --max_total_decrypt <value>  Maximum allowed total decrypt time in microseconds (default: 0)
  -i, --input <input>              Name of the input datafile in json format (default: null)
  -c, --credentials <CREDENTIALS>  Set the file name with the API credentials (default: ~/.ubiq/credentials) (default: null)
  -P, --profile <PROFILE>          Identify the profile within the credentials file (default: default (default: null)
  -h, --help                       display help for command

```

### Sample execution


```console
# Supplied parameters expect this dataset to be processed and
#     The average encrypt operation to succeed in less 2000 microseconds.
#     The average decrypt operation to succeed in less 2000 microseconds.
#     The total encrypt time for the entire datafile to take less than 2000000 microseconds (2 s)
#     The total decrypt time for the entire datafile to take less than 2000000 microseconds (2 s)


node load_test.js -i somefile-1000.json  -c ./credentials.txt -p -e 2000 -d 2000 -E 2000000 -D 2000000
Processing record: 0
All data validated
Encrypt records count 1000.  Times in (microseconds)
	Dataset: UTF8_STRING_COMPLEX, record_count: 250, Average: 2298, total 574595
	Dataset: ALPHANUM_SSN, record_count: 265, Average: 597, total 158091
	Dataset: SSN, record_count: 223, Average: 1696, total 378235
	Dataset: BIRTH_DATE, record_count: 262, Average: 1747, total 457680
	  Total: Average: 1569, total 1568601

Decrypt records count 1000.  Times in (microseconds)
	Dataset: UTF8_STRING_COMPLEX, record_count: 250, Average: 2285, total 571266
	Dataset: ALPHANUM_SSN, record_count: 265, Average: 606, total 160459
	Dataset: SSN, record_count: 223, Average: 1744, total 388807
	Dataset: BIRTH_DATE, record_count: 262, Average: 1812, total 474751
	  Total: Average: 1595, total 1595283
PASSED: Maximum allowed average encrypt threshold of 2000 microseconds
PASSED: Maximum allowed average decrypt threshold of 2000 microseconds
PASSED: Maximum allowed total encrypt threshold of 2000000 microseconds
PASSED: Maximum allowed total decrypt threshold of 2000000 microseconds
```

[dashboard]:https://dashboard.ubiqsecurity.com/
[credentials]:https://dev.ubiqsecurity.com/docs/how-to-create-api-keys
[apidocs]:https://dev.ubiqsecurity.com/docs/api
[npm]:https://www.npmjs.com
[dashboard]:https://dev.ubiqsecurity.com/docs/dashboard
[yarn]: https://yarnpkg.com/
