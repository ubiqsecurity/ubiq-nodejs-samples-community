import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class poc_app {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column('varchar', { length: 15, nullable: true })
  ssn_sensitive: string;

  @Column('varchar', { length: 100, nullable: true })
  full_name_sensitive: string;

  @Column('varchar', { length: 15, nullable: true })
  user_preferences: string;

  @Column('varchar', { length: 25, nullable: true })
  phone_number_sensitive: string;

  @Column('varchar', { length: 10, nullable: true })
  contact_method: string;

  @Column('varchar', { length: 100, nullable: true })
  email_sensitive: string;
}
