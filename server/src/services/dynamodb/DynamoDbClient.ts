import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
const REGION = process.env.DYNAMO_REGION;

const DynamoDbClient = new DynamoDBClient({
  region: REGION,
  endpoint:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:8000'
      : undefined,
  credentials: {
    accessKeyId: `${process.env.DYNAMO_ACCESS_KEY_ID}`,
    secretAccessKey: `${process.env.DYNAMO_SECRET_ACCESS_KEY}`,
  },
});

export { DynamoDbClient };
