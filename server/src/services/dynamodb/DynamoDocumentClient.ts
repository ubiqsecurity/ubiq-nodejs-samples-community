import { DynamoDBDocumentClient } from '@aws-sdk/lib-dynamodb';
import { DynamoDbClient } from './DynamoDbClient';

const marshallOptions = {
  convertEmptyValues: false,
  removeUndefinedValues: false,

  convertClassInstanceToMap: false,
};

const unmarshallOptions = {
  wrapNumbers: false,
};

const translateConfig = { marshallOptions, unmarshallOptions };

const ddbDocClient = DynamoDBDocumentClient.from(
  DynamoDbClient,
  translateConfig
);

export { ddbDocClient };
