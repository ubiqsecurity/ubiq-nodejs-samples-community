import { ddbDocClient } from './DynamoDocumentClient';
import { PutCommand, ScanCommand } from '@aws-sdk/lib-dynamodb';
import { tableRow } from '../../types';
import { v4 as uuidv4 } from 'uuid';

export const params = {
  TableName: process.env.DYNAMO_TABLE_NAME,
};

export const getItems = async () => {
  try {
    const data = await ddbDocClient.send(new ScanCommand(params));
    return data.Items;
  } catch (err) {
    console.log('Error', err);
  }
};

export const saveItem = async (item: tableRow) => {
  try {
    item.id = uuidv4();
    await ddbDocClient.send(
      new PutCommand({
        TableName: params.TableName,
        Item: item,
      })
    );
  } catch (err) {
    console.log('Error', err);
  }
};
