const ubiq = require('ubiq-security');
import { ubiqCredentials } from '../index';
import { poc_app } from '../entity/Table';

const ffsMap = {
  ssn_sensitive: 'SSN',
  full_name_sensitive: 'GENERIC_ALPHANUMERIC',
  phone_number_sensitive: 'PHONE',
  email_sensitive: 'EMAIL',
};

// Using Bulk Interface
export async function ubiqDecrypt(encryptedTableData: Array<poc_app>) {
  const ubiqEncryptDecrypt = new ubiq.fpeEncryptDecrypt.FpeEncryptDecrypt({
    ubiqCredentials,
  });
  let decryptedTableData: poc_app[] = [];
  for (const encryptedRow of encryptedTableData) {
    const isRowSensitive = Object.keys(encryptedRow).some((rowKey) =>
      rowKey.includes('sensitive')
    );
    if (!isRowSensitive) {
      decryptedTableData.push(encryptedRow);
      continue;
    }
    let decryptedRow = encryptedRow;
    for (const key in encryptedRow) {
      if (key.includes('sensitive')) {
        const ffsName = ffsMap[key];
        const data = encryptedRow[key];
        const decryptedItem = await ubiqEncryptDecrypt.DecryptAsync(
          ffsName,
          data
        );

        decryptedRow[key] = decryptedItem;
      }
    }
    decryptedTableData.push(decryptedRow);
  }
  return decryptedTableData;
}

// Using Simple Interface
export async function ubiqEncrypt(unencryptedRow: poc_app) {
  const isRowSensitive = Object.keys(unencryptedRow).some((rowKey) =>
    rowKey.includes('sensitive')
  );
  if (!isRowSensitive) {
    return unencryptedRow;
  }
  let encryptedRow = unencryptedRow;
  for (const key in unencryptedRow) {
    if (key.includes('sensitive')) {
      const ffsName = ffsMap[key];
      const data = unencryptedRow[key];
      const encryptedItem = await ubiq.fpeEncryptDecrypt.Encrypt({
        ubiqCredentials,
        ffsname: ffsName,
        data: data,
      });

      encryptedRow[key] = encryptedItem;
    }
  }
  return encryptedRow;
}
