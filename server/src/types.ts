import { poc_app } from './entity/Table';

export interface tableRow extends Omit<poc_app, 'id'> {
  id: number | string;
}
