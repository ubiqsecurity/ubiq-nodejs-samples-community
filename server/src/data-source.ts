import 'reflect-metadata';
import { DataSource } from 'typeorm';
import { poc_app } from './entity/Table';

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: process.env.DB_HOST,
  port: 3306,
  username: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: process.env.DB_NAME,
  synchronize: true,
  logging: false,
  entities: [poc_app],
  migrations: [],
  subscribers: [],
});
