import { port } from './config';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import { Request, Response } from 'express';
import { AppDataSource } from './data-source';
import { Routes } from './routes';
import { getItems } from './services/dynamodb/DynamoDb';
const ubiq = require('ubiq-security');
export const ubiqCredentials = new ubiq.Credentials();
var cors = require('cors');

AppDataSource.initialize()
  .then(async () => {
    // create express app
    const app = express();
    app.use(cors());
    app.use(bodyParser.json());

    app.use(morgan('tiny'));

    // register express routes from defined application routes
    Routes.forEach((route) => {
      (app as any)[route.method](
        route.route,
        async (req: Request, res: Response, next: Function) => {
          try {
            const result = await new (route.controller as any)()[route.action](
              req,
              res,
              next
            );
            res.json({ data: result });
          } catch (err) {
            next(err);
          }
        }
      );
    });

    // setup express app here
    // ...

    // app.get('/api', async (req, res) => {
    //   const data = await getItems();
    //   res.json({ data: data });
    // });

    // start express server
    app.listen(port);

    console.log(`Express server has started on port ${port}.`);
  })
  .catch((error) => console.log(error));
