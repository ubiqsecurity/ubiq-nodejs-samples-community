import { NextFunction, Request, Response } from 'express';
import { poc_app } from '../entity/Table';
import { AppDataSource } from '../data-source';
import { ubiqDecrypt, ubiqEncrypt } from '../services/Ubiq';
import { getItems, saveItem } from '../services/dynamodb/DynamoDb';

export class TableController {
  private tableRepository = AppDataSource.getRepository(poc_app);

  async getEncryptedTableData(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    const database = request.params.database;
    try {
      if (database === 'mysql') {
        const encryptedTableData = this.tableRepository.find();
        return encryptedTableData;
      } else if (database === 'dynamodb') {
        return await getItems();
      } else {
        throw new Error('Database not supported');
      }
    } catch (err) {
      console.log('Error', err);
    }
  }

  async getDecryptedTableData(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    const database = request.params.database;
    try {
      if (database === 'mysql') {
        const encryptedTableData = await this.tableRepository.find();
        const decryptedTableData = await ubiqDecrypt(encryptedTableData);
        return decryptedTableData;
      } else if (database === 'dynamodb') {
        const encryptedTableData = await getItems();
        const decryptedTableData = await ubiqDecrypt(
          encryptedTableData as poc_app[]
        );
        return decryptedTableData;
      } else {
        throw new Error('Database not supported');
      }
    } catch (err) {
      console.error(err.message);
    }
  }

  async save(request: Request, response: Response, next: NextFunction) {
    const database = request.params.database;
    try {
      const encryptedRow = await ubiqEncrypt(request.body);
      if (database === 'mysql') {
        return this.tableRepository.save(encryptedRow);
      } else if (database === 'dynamodb') {
        return await saveItem(encryptedRow);
      }
    } catch (err) {
      console.error(err.message);
    }
  }

  // async remove(request: Request, response: Response, next: NextFunction) {
  //   let rowToRemove = await this.tableRepository.findOneBy({
  //     id: request.params.id,
  //   });
  //   if (rowToRemove) await this.tableRepository.remove(rowToRemove);
  // }
}
