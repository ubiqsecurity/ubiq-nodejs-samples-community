import { TableController } from './controller/TableController';
export const Routes = [
  {
    method: 'get',
    route: '/api/poc-app-data/:database',
    controller: TableController,
    action: 'getEncryptedTableData',
  },
  {
    method: 'get',
    route: '/api/poc-app-data/:database/decrypt',
    controller: TableController,
    action: 'getDecryptedTableData',
  },
  {
    method: 'post',
    route: '/api/poc-app-data/:database',
    controller: TableController,
    action: 'save',
  },
  {
    method: 'delete',
    route: '/api/poc-app-data/:id',
    controller: TableController,
    action: 'remove',
  },
];
